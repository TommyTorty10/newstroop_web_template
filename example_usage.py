from NEWSTROOP_web_template.nttemplates import *

path_to_articles_dir = '/dir'
index_html_path = '/index.html'
repo = 'github_username/repo'
branchname = 'master'
credential_file = 'config.txt'

headline = "hey, I'm a headline"
article_body = "yo, I'm an article"
description = "small article"

# fill in article template and return the number of the new article
article_number = fill_article(path_to_articles_dir,
                              headline,
                              article_body)

# fill in the card template
fill_card(article_number, headline, description, index_html_path)

# upload article
file_to_upload = path_to_articles_dir + '/' + str(article_number) + '.html'
upload(file_to_upload, repo, credential_file, branchname, 'optional commit msg')

# upload card
file_to_upload = index_html_path
upload(file_to_upload, repo, credential_file, branchname, 'optional commit msg')
