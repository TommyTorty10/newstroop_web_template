# [NEWS TROOP HTML Template Filling Automation](https://sites.google.com/view/newstroop/home)

### Index
1. [Installation](#installation)
2. [Usage](#usage)
3. [License](#copyright-and-license)

## Installation
* open a terminal
* git clone https://github.com/ABlueTortoise30/NEWSTROOP_web_template.git
* navigate into NEWSTROOP_web_template/
* pip3 install .

This executes setup.py and installs the module. 
NOTE, wherever you clone the repo to is where pip will look for its source, so you cannot delete it after
installing it with pip unless you reinstall it from another location.  

#### Github Credentials
To upload files to Github you must have a file containing a Github access token on a new line. 
The token makes accessing Github more secure, and you can learn how to 
[get a token here.](https://help.github.com/en/github/authenticating-to-github/creating-a-personal-access-token-for-the-command-line)  
Depending on your system, your OS may have specific secure files for this sort of purpose.  
The file should follow the following format:
```bash
--other secure stuff--
GITHUB_TOKEN=blahblah
--possibly other stuff--
```

## Usage
Below is a more verbose version of example_usage.py provided for comprehension.  
You can use example_usage.py or the code below, but both require certain information to be filled in.

```python
from NEWSTROOP_web_template.nttemplates import *

path_to_articles_dir = '/dir'
index_html_path = '/index.html'
repo = 'github_username/repo'
branchname = 'master'
credential_file = 'config.txt'

headline = "hey, I'm a headline"
article_body = "yo, I'm an article"
description = "small article"

# fill in article template and return the number of the new article
article_number = fill_article(path_to_articles_dir,
                              headline,
                              article_body)

# fill in the card template
fill_card(article_number, headline, description, index_html_path)

# upload article
file_to_upload = path_to_articles_dir + '/' + str(article_number) + '.html'
upload(file_to_upload, repo, credential_file, branchname, 'optional commit msg')

# upload card
file_to_upload = index_html_path
upload(file_to_upload, repo, credential_file, branchname, 'optional commit msg')
```
#### fill_article()
Will fill in an article template and write to a new file, gx.html, in the provided articles folder.

Parameters:
* articles_path:
    * type: string
    * is the absolute path to the directory of articles

* headline:
    * type: string
    * is the headline of the article

* body:
    * type: string
    * is the body text of the article
    
#### fill_card()
Will fill in an card template and insert the card into the index.html provided.  
The card is inserted below the `<main>` tag.

Parameters:
* art_num:
    * type: int
    * is the number that the current article will be

* art_title:
    * type: string
    * is the headline of the article

* description:
    * type: string
    * is a description or first line of the article

* index_html_path:
    * type: string
    * is the absolute path to the index.html file of the website
    * NOTE keep a backup of the index.html file!

#### upload()
Will upload a provided file to a provided Github repository.
Parameters:
* file:
    * type: string
    * is the absolute path to the file to upload

* repo:
    * type: string
    * is in the form githubusername/repo

* cred_path:
    * type: string
    * is the absolute path to a text file with an authorization token in the first line

* branch:
    * type: string
    * is the branch of the repository to upload to

* commit:
    * type: string
    * not required
    * is the commit message


## Copyright and License
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">NEWS TROOP HTML Template Filling Automation</span> 
by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/TommyTorty10" property="cc:attributionName" rel="cc:attributionURL">TommyTorty10</a> is licensed under a 
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://github.com/minimaxir/textgenrnn" rel="dct:source">https://github.com/minimaxir/textgenrnn</a>.
