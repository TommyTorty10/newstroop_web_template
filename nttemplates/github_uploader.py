from github import Github
from os.path import split
# from os import listdir


def upload(file, repo, cred_path, branch, commit_msg='added new article'):
    """
    Will upload a provided file to a provided Github repository.

    Parameters:
        file:
            type: string
            is the absolute path to the file to upload

        repo:
            type: string
            is in the form githubusername/repo

        cred_path:
            type: string
            is the absolute path to a text file with an authorization token in the first line

        branch:
            type: string
            is the branch of the repository to upload to

        commit_msg:
            type: string
            not required
            is the commit message

"""
    # get file contents
    with open(file, 'r') as f:
        file_contents = f.read()

    # get Github credentials
    with open(cred_path, 'r') as f:
        cred_file = f.readlines()

    token = ''
    for line in cred_file:
        if 'GITHUB_TOKEN' in line:
            token = line
            break

    # declare Github stuff
    token = token.split('=')[1].rstrip()
    g = Github(token)
    repository = g.get_repo(repo)

    # parse different paths locally and in repo
    dir_tree, filename = split(file)  # get filename from file path
    repo_root = split(repo)[1]
    repo_dirs = file.split('/')
    root_index = repo_dirs.index(repo_root)
    repo_indexed_filepath = repo_dirs[root_index+2:]
    repo_indexed_filepath = '/'.join(repo_indexed_filepath)  # looks like articles/032620-4.html

    # [[ HOW THE GIT FUNCTIONS WORK ]]
    # contents = repository.get_contents("index.html", ref=branch)
    # repository.update_file(contents.path, 'commit name', 'file contents', sha=contents.sha, branch='test')
    # repository.create_file('index.html', 'commit name', 'file contents', branch='test')

    # check if file exits; if so, update it; if not, create it
    contents = repository.get_contents('')
    isitthere = False
    for f in contents:
        if filename in str(f):
            isitthere = True
            update_contents = repository.get_contents(repo_indexed_filepath, ref=branch)
            repository.update_file(update_contents.path, commit_msg, file_contents,
                                   sha=update_contents.sha, branch=branch)

            print('uploaded pre-existing: ', filename)
            break

    if not isitthere:
        # print('local', repo_indexed_filepath)
        repository.create_file(repo_indexed_filepath, commit_msg, file_contents, branch=branch)
        print('uploaded new: ', repo_indexed_filepath)
