from os import path
from github import Github


months = ['January', 'February', 'March', 'April', 'May', 'June',
          'July', 'August', 'September', 'October', 'November', 'December']
templates_path = path.split(__file__)[0] + '/templates'


def locate_regex(article_path, regex):
    """
    Parameters:
        article_path:
            type: string
            is the absolute path to the file of the article

        regex:
            type: string
            is the string to search for to index what to find

"""
    # print(article_path)
    # print('---'*5, '\n', regex)
    # print('---' * 5, '\n')

    with open(article_path, 'r') as f:
        article = f.readlines()

    i = True
    counter = 0
    while i:
        if regex in article[counter]:
            i = False
        counter += 1
    return article[counter], counter
