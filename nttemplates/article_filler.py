from .misc import *
import os
from random import sample
from datetime import datetime
from os import listdir


def fill_article(articles_path, headline, body):
    """
    Will fill in an article template and write to a new file, gx.html, in the provided articles folder.


    Parameters:
        articles_path:
            type: string
            is the absolute path to the directory of articles

        headline:
            type: string
            is the headline of the article

        body:
            type: string
            is the body text of the article

"""
    # determine name
    date = datetime.today()
    suffixes = []
    tracker = False
    # create base name assuming it is the first post of the day
    final_art_name = '{:02}'.format(date.month) + '{:02}'.format(date.day) + str(date.year)[-2:]
    # check if there are other posts of the same name and iterate
    for i in listdir(articles_path):
        # print('all', i)
        if final_art_name in i:
            tracker = True
            # print('outer', i, i[7])
            try:
                suffix_num = int(i[7])
                # print('finally', i)
                suffixes.append(suffix_num)
            except:
                pass

    # append a suffix in format date-x if necessary
    if tracker:
        try:
            suffix = max(suffixes) + 1
            final_art_name = final_art_name + '-' + str(suffix)
        except:
            suffix = 1
            final_art_name = final_art_name + '-' + str(suffix)

    # edit the template
    with open(templates_path+'/article.html', 'r') as f:
        html_template = f.readlines()

    final_art_name = final_art_name + '.html'

    # write in article number
    html_template[36] = final_art_name[:-5]

    # select 3 random old articles to advertise
    old_arts = listdir(articles_path)
    ad_arts = sample(old_arts, 3)
    html_template[42], html_template[49], html_template[56] = ['g'+str(ad_arts[i])+'\n' for i in range(3)]

    # copy headlines of ad_arts ^ to 45 52 59
    # regex = '<!-- headline copies first sentence from article -->'
    regex = '<!-- headline copies first sentence from article'
    h1, c = locate_regex(articles_path+'/'+str(ad_arts[0]), regex)
    h2, c = locate_regex(articles_path+'/'+str(ad_arts[1]), regex)
    h3, c = locate_regex(articles_path+'/'+str(ad_arts[2]), regex)
    html_template[44] = h1
    html_template[51] = h2
    html_template[58] = h3

    # format line with author and date
    date_str = months[date.month] + ' ' + str(date.day) + ', ' + str(date.year)
    html_template[71] = 'By author' + html_template[71].strip('\n') + str(date_str) + '\n'
    print(html_template[71])

    # insert new headline
    html_template[65] = headline + '\n'

    # insert new body
    html_template[75] = '5'
    bod = str(body)
    bod = '<p>' + bod + '</p>'
    newlinestr = '</p>\n\n<p>'
    bod = newlinestr.join(bod.splitlines())
    html_template[75] = '\n'+bod+'\n\n'

    # write new article into new file
    write_loc = articles_path + '/' + final_art_name
    with open(write_loc, 'w') as f:
        f.writelines(html_template)

    return final_art_name[:-5]
