from .misc import *
from datetime import datetime


def fill_card(art_num, art_title, description, index_html_path):
    """
    Will fill in an card template and insert the card into the index.html provided.
    The card is inserted below the `<main>` tag.

    Parameters:
        art_num:
            type: int
            is the number that the current article will be

        art_title:
            type: string
            is the headline of the article

        description:
            type: string
            is a description or first line of the article

        index_html_path:
            type: string
            is the absolute path to the index.html file of the website
            NOTE keep a backup of the index.html file!

"""

    with open(templates_path+'/card.html', 'r') as f:
        l = f.readlines()

    # write in article number
    g, c = locate_regex(templates_path+'/card.html', '<!-- article # -->')
    l[c+1] = art_num + '\n'
    l[c+6] = l[c+1]

    # write in article title
    l[c+8] = art_title + '\n'

    # write in article description
    l[c+12] = description + '\n'

    # write in the date
    date = datetime.now()
    date_str = months[date.month] + ' ' + str(date.day) + ', ' + str(date.year)
    l[c+15] = date_str + '\n'

    # write card to index.html
    l.insert(0, '\n\n\n')
    l.append('\n\n\n')

    with open(index_html_path, 'r') as f:
        index_html = f.readlines()

    r, c = locate_regex(index_html_path, '<main>')

    for i in range(len(l)):
        index_html.insert(c+i, l[i])

    with open(index_html_path, 'w') as f:
        f.writelines(index_html)
